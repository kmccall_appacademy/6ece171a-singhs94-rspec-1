def ftoc(num)
  result = (num - 32) * 5/9
  return result
end

def ctof(num)
  result = (num * 9/5.round(1)) + 32
  return result
end
