def add(x,y)
  return x + y
end

def subtract(x,y)
  return x - y
end

def sum(array)
  result = 0;

  array.each do |x|
    result = result + x
  end
  return result
end
