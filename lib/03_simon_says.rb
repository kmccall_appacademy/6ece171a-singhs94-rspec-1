def echo(string)
  return string
end

def shout(string)
  return string.upcase
end

def repeat(string, number = 2)
  amount = number - 1
  build = string
  amount.times {build += " #{string}"}
  return build
end

def start_of_word(string,letters)
  return string[0, letters]
end

def first_word(string)
  words = string.split(' ')
  return words[0]
end

def titleize(string)
  array = string.split(' ')
  little_words = ["the","and","over"]
  title = []
  title.push(array[0].capitalize)
  array[1..-1].each do |x|
    if little_words.include?(x)
      title.push(x)
    else
      title.push(x.capitalize)
    end
  end

  return title.join(' ')
end
