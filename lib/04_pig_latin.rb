def translate(string)
  vowel = ['a','e','i','o','u']
  phoneme = ['qu']

  array = string.split(' ')

  build = []
  i = 0
  j = 0
  while i < array.length
    result = ''
    result2 = ''
      while j < array[i].length
        if phoneme.include?(array[i][j..j+1])
          result2 = result2 + 'qu'
        elsif vowel.include?(array[i][j]) && phoneme.include?(array[i][j-1] + array[i][j]) == false
          result = array[i][j..-1]
          break
        elsif vowel.include?(array[i][j]) == false
          result2 = result2 + array[i][j]
        end
      j = j + 1
      end
    build.push(result + result2 + 'ay')
    i = i + 1
    j = 0;
  end

  return build.join(' ')

end
